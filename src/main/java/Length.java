public class Length
{
    double value;

    public Length(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }
}
