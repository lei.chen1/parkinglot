package parkinglot;

import parkinglot.NonParkingPlaceException;
import parkinglot.ParkingAssistant;
import parkinglot.ParkingLot;

import java.util.Comparator;
import java.util.List;

public class ParkingAssistantC extends ParkingAssistant {
    public ParkingAssistantC(int... parkingLotSize) {
        super(parkingLotSize);
    }

    @Override
    protected ParkingLot selectParkingLot(List<ParkingLot> parkingLotList) throws NonParkingPlaceException {
        return parkingLotList.stream()
                .max(ParkingLot::compareTo)
                .orElseThrow(NonParkingPlaceException::new);
    }
}
