package parkinglot;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ParkingLot implements Comparable<ParkingLot> {
    private int used;
    private Map<Integer, Car> carMap = new HashMap<>();
    private int size;
    private int id;

    public int getId() {
        return id;
    }

    public ParkingLot(int id, int size) {
        this.size = size;
        this.id = id;
    }

    public ParkingLot() {
        this(0, 10);
    }

    public ParkingCard parking(Car car) throws NonParkingPlaceException {
        if (carMap.size() == size) {
            throw new NonParkingPlaceException();
        }

        carMap.put(used++, car);
        return new ParkingCard(id, used - 1, car.getNum());
    }

    private Car pickUp(int cardNumber) throws InvalidCardNumberException {
        return Optional.ofNullable(carMap.remove(cardNumber)).orElseThrow(InvalidCardNumberException::new);
    }

    public Car pickUp(ParkingCard parkingCard) throws InvalidCardNumberException {
        Optional.ofNullable(parkingCard).orElseThrow(InvalidCardNumberException::new);
        return pickUp(parkingCard.getNumber());
    }

    public int getRemainSize() {
        return size - carMap.size();
    }

    public double getRemainPercent() {
        return 1 - ((double)carMap.size() / (double)size);
    }

    @Override
    public int compareTo(ParkingLot o) {
        if (getRemainPercent() == o.getRemainPercent()) {
            return Integer.compare(getRemainSize(), o.getRemainSize());
        }
        return Double.compare(getRemainPercent(), o.getRemainPercent());
    }
}
