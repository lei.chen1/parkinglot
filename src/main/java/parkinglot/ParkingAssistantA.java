package parkinglot;

import java.util.Comparator;
import java.util.List;

public class ParkingAssistantA extends ParkingAssistant {
    public ParkingAssistantA(int... parkingLotSize) {
        super(parkingLotSize);
    }

    @Override
    protected ParkingLot selectParkingLot(List<ParkingLot> parkingLotList) throws NonParkingPlaceException {
        return parkingLotList.stream()
                .max(Comparator.comparing(ParkingLot::getRemainSize, Integer::compareTo))
                .orElseThrow(NonParkingPlaceException::new);
    }
}
