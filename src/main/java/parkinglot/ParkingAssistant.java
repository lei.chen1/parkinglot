package parkinglot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class ParkingAssistant {
    private List<ParkingLot> parkingLotList;

    public ParkingAssistant(int ... parkingLotSize) {
        parkingLotList = new ArrayList<>();
        for (int i = 0; i < parkingLotSize.length; i++) {
            parkingLotList.add(new ParkingLot(i, parkingLotSize[i]));
        }
    }

    protected abstract ParkingLot selectParkingLot(List<ParkingLot> parkingLotList) throws NonParkingPlaceException;

    public ParkingCard parking(Car car) throws NonParkingPlaceException {
        ParkingLot parkingLot = selectParkingLot(parkingLotList);
        return parkingLot.parking(car);
    }

    public Car pickUp(ParkingCard parkingCard) throws InvalidCardNumberException {
        Optional<ParkingLot> parkingLotOptional = parkingLotList.stream()
                .filter(o -> o.getId() == parkingCard.getParkingLotId())
                .findFirst();
        parkingLotOptional.orElseThrow(InvalidCardNumberException::new);
        return parkingLotOptional.get().pickUp(parkingCard);
    }
}
