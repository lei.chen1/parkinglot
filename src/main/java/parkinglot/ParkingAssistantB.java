package parkinglot;

import java.util.Comparator;
import java.util.List;

public class ParkingAssistantB extends ParkingAssistant {
    public ParkingAssistantB(int... parkingLotSize) {
        super(parkingLotSize);
    }

    @Override
    protected ParkingLot selectParkingLot(List<ParkingLot> parkingLotList) throws NonParkingPlaceException {
        return parkingLotList.stream()
                .max(Comparator.comparing(ParkingLot::getRemainPercent, Double::compareTo))
                .orElseThrow(NonParkingPlaceException::new);
    }
}
