package parkinglot;

import java.util.Objects;

public class Car {
    private int num;

    public Car(int num) {
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return num == car.num;
    }

    @Override
    public int hashCode() {
        return Objects.hash(num);
    }
}
