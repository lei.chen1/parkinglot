package parkinglot;

public class ParkingCard {
    private int parkingLotId;
    private int number;
    private int carNum;

    public ParkingCard(int parkingLotId, int number, int carNum) {
        this.parkingLotId = parkingLotId;
        this.number = number;
        this.carNum = carNum;
    }

    public int getParkingLotId() {
        return parkingLotId;
    }

    public int getNumber() {
        return number;
    }

    public int getCarNum() {
        return carNum;
    }
}
