import org.junit.Test;
import parkinglot.Car;
import parkinglot.InvalidCardNumberException;
import parkinglot.NonParkingPlaceException;
import parkinglot.ParkingAssistant;
import parkinglot.ParkingAssistantA;
import parkinglot.ParkingAssistantB;
import parkinglot.ParkingAssistantC;
import parkinglot.ParkingCard;
import parkinglot.ParkingLot;

import static org.junit.Assert.assertEquals;

public class ParkingLotTest {

    @Test
    public void testShouldReturnParkingCardWhenParking() throws NonParkingPlaceException {
        ParkingLot parkingLot = new ParkingLot();
        ParkingCard card = parkingLot.parking(new Car(1));
        assertEquals(0, card.getParkingLotId());
        assertEquals(0, card.getNumber());
        assertEquals(1, card.getCarNum());
    }

    @Test
    public void testShouldReturnCarWhenPickup() throws NonParkingPlaceException, InvalidCardNumberException {
        Car myCar = new Car(1);
        ParkingLot parkingLot = new ParkingLot();
        ParkingCard parkingCard = parkingLot.parking(myCar);
        Car car = parkingLot.pickUp(parkingCard);
        assertEquals(car, myCar);
    }

    @Test
    public void should_return_card_number_when_parking_lot_have_last_one() throws NonParkingPlaceException {
        ParkingLot parkingLot = new ParkingLot();
        for (int i = 0; i < 9; i++) {
            parkingLot.parking(new Car(1));
        }
        assertEquals(9, parkingLot.parking(new Car(1)).getNumber());
    }

    @Test(expected = NonParkingPlaceException.class)
    public void should_throw_NonParkingPlaceException_when_parking_lot_is_full() throws NonParkingPlaceException {
        ParkingLot parkingLot = new ParkingLot(0, 1);
        parkingLot.parking(new Car(1));
        parkingLot.parking(new Car(2));
    }

    @Test(expected = InvalidCardNumberException.class)
    public void should_throw_InvalidCarNoException_when_pickup_with_invalid_card_number() throws InvalidCardNumberException {
        ParkingLot parkingLot = new ParkingLot();
        parkingLot.pickUp(null);
    }

    @Test(expected = InvalidCardNumberException.class)
    public void should_throw_InvalidCarNoException_when_using_the_same_cardNumber_pickup_two_times() throws NonParkingPlaceException, InvalidCardNumberException {
        ParkingLot parkingLot = new ParkingLot();
        ParkingCard parkingCard = parkingLot.parking(new Car(1));
        parkingLot.pickUp(parkingCard);
        parkingLot.pickUp(parkingCard);
    }

    @Test
    public void should_parking_car() throws NonParkingPlaceException {
        ParkingAssistant parkingLot = new ParkingAssistantA(1, 2, 3);
        ParkingCard parkingCard = parkingLot.parking(new Car(2));
        assertEquals(2, parkingCard.getParkingLotId());
        ParkingCard parkingCard1 = parkingLot.parking(new Car(3));
        assertEquals(1, parkingCard1.getParkingLotId());
    }

    @Test
    public void should_pick_up_car() throws NonParkingPlaceException, InvalidCardNumberException {
        ParkingAssistant parkingLot = new ParkingAssistantA(1, 2, 3);
        Car car = new Car(2);
        ParkingCard parkingCard = parkingLot.parking(car);
        assertEquals(car, parkingLot.pickUp(parkingCard));
    }

    @Test
    public void should_parking_car_use_assistant_b() throws NonParkingPlaceException {
        ParkingAssistant parkingLot = new ParkingAssistantB(1, 2, 3);
        ParkingCard parkingCard = parkingLot.parking(new Car(2));
        assertEquals(0, parkingCard.getParkingLotId());
        assertEquals(1, parkingLot.parking(new Car(1)).getParkingLotId());
        assertEquals(2, parkingLot.parking(new Car(3)).getParkingLotId());
    }

    @Test
    public void should_parking_car_use_assistant_c() throws NonParkingPlaceException {
        ParkingAssistant parkingLot = new ParkingAssistantC(1, 2, 3);
        ParkingCard parkingCard = parkingLot.parking(new Car(2));
        assertEquals(2, parkingCard.getParkingLotId());
        assertEquals(1, parkingLot.parking(new Car(1)).getParkingLotId());
        assertEquals(0, parkingLot.parking(new Car(3)).getParkingLotId());
    }
}
